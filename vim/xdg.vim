set directory=$XDG_STATE_HOME/vim/swap
set backupdir=$XDG_STATE_HOME/vim/backup
set undodir=$XDG_STATE_HOME/vim/undo
set viminfofile=$XDG_STATE_HOME/vim/viminfo
set runtimepath=$XDG_DATA_HOME/vim,$XDG_DATA_HOME/vim/after,$VIM,$VIMRUNTIME
let confdir = "$XDG_DATA_HOME/vim"
let datadir = "$XDG_DATA_HOME/vim"
