local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
    "nvim-lualine/lualine.nvim",
    { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },
    { "folke/tokyonight.nvim", lazy = false, priority = 1000, opts = {}, },
    "neovim/nvim-lspconfig",
    { "williamboman/mason.nvim", cmd = "MasonUpdate" },
    "williamboman/mason-lspconfig.nvim",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/nvim-cmp",
    { "L3MON4D3/LuaSnip", version = "v2.*" },
    "saadparwaiz1/cmp_luasnip",
    "tpope/vim-fugitive",
    {
        "iamcco/markdown-preview.nvim",
        cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
        ft = { "markdown" },
        build = function() vim.fn["mkdp#util#install"]() end,
    },
    {
        "NeogitOrg/neogit",
        dependencies = {
            "nvim-lua/plenary.nvim",         -- required
            "sindrets/diffview.nvim",        -- optional - Diff integration

            -- Only one of these is needed, not both.
            "nvim-telescope/telescope.nvim", -- optional
        },
        config = true
    }
}
require("lazy").setup(plugins)

--[[ material colorscheme config ]]--
vim.opt.termguicolors = true
vim.g.material_style = "palenight"
require("lualine").setup({
    options = {
        theme = "tokyonight"
    }
})
vim.cmd([[colorscheme tokyonight-storm]])

--[[ enable mason ]]--
require("mason").setup()
require("mason-lspconfig").setup()

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>f', function()
      vim.lsp.buf.format { async = true }
    end, opts) end,
})

--[[ treesitter ]]--
require("nvim-treesitter.configs").setup({
    ensure_installed = { },
    sync_install = false,
    auto_install = true,
    modules = {},
    ignore_install = {},
    highlight = {
        enable = true,
    },
    incremental_selection = {
        enable = true,
        keymaps = {
                init_selection = "gnn", -- set to `false` to disable one of the mappings
                node_incremental = "grn",
                scope_incremental = "grc",
                node_decremental = "grm",
        },
    }
})

--[[ autocompletion with cmp  ]]--
require("cmp").setup({
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end,
    },
    mapping = require("cmp").mapping.preset.insert({
        ["<Tab>"] = require("cmp").mapping.select_next_item(),
        ["<S-Tab>"] = require("cmp").mapping.select_prev_item(),
        ["<C-b>"] = require("cmp").mapping.scroll_docs(-4),
        ["<C-f>"] = require("cmp").mapping.scroll_docs(4),
        ["<C-Space>"] = require("cmp").mapping.complete(),
        ["<C-e>"] = require("cmp").mapping.abort(),
        ["<CR>"] = require("cmp").mapping.confirm({ select = false }),
    }),
    sources = require("cmp").config.sources({
        { name = "nvim_lsp" },
        { name = "luasnip" },
    }, {
        { name = "buffer" },
    }),
    experimental = {
        ghost_text = true
    }
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won"t work anymore).
require("cmp").setup.cmdline({ "/", "?" }, {
    mapping = require("cmp").mapping.preset.cmdline(),
    sources = {
        { name = "buffer" }
    }
})

-- Use cmdline & path source for ":" (if you enabled `native_menu`, this won"t work anymore).
require("cmp").setup.cmdline(":", {
    mapping = require("cmp").mapping.preset.cmdline(),
    sources = require("cmp").config.sources({
        { name = "path" }
    }, {
        { name = "cmdline" }
    })
})

-- Set up lspconfig.
local capabilities = require("cmp_nvim_lsp").default_capabilities()
local mason_lspconfig_handlers = {
    -- The first entry (without a key) will be the default handler
    -- and will be called for each installed server that doesn't have
    -- a dedicated handler.
    function (server_name) -- default handler (optional)
        require("lspconfig")[server_name].setup({
            capabilities = capabilities
        })
    end,

    ["lua_ls"] = function ()
        require("lspconfig").lua_ls.setup({
            capabilities = capabilities,
            settings = {
                Lua = {
                    runtime = {
                        -- Tell the language server which version of Lua you"re using (most likely LuaJIT in the case of Neovim)
                        version = "LuaJIT",
                    },
                    diagnostics = {
                        -- Get the language server to recognize the `vim` global
                        globals = {"vim"},
                    },
                    workspace = {
                        -- Make the server aware of Neovim runtime files
                        library = vim.api.nvim_get_runtime_file("", true),
                        checkThirdParty = false
                    },
                    --   Do not send telemetry data containing a randomized but unique identifier
                    telemetry = {
                        enable = false,
                    },
                },
            },
        })
    end,

    ["bashls"] = function ()
        require("lspconfig").bashls.setup({
            capabilities = capabilities,
            filetypes = {
                "sh",
                "bash",
                "zsh"
            }
        })
    end
}

require("mason-lspconfig").setup_handlers(mason_lspconfig_handlers)

--[[ this is not supported by Mason ]]--
require("lspconfig").ccls.setup({
    capabilities = capabilities,
    init_options = {
        cache = {
            directory = ".ccls-cache";
        }
    }
})

--[[ disable hecking mouse ]]--
vim.opt.mouse = "nv"
vim.opt.mousemodel = "extend"

--[[ normal tab ]]--
vim.opt.smarttab = true
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

--[[ linenums ]]--
vim.opt.number = true
vim.opt.relativenumber = true

--[[ hideen ]]--
vim.opt.hidden = true

--[[ manpager ]]--
vim.g.man_hardwrap = 0

--[[ persistent undo ]]--
vim.opt.undofile = true

--[[ Gentoo stuff ]]--
vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
    pattern = { vim.fn.expand("~") .. "/dev/gentoo/*", vim.fn.expand("~") .. "/dev/pidruchniki/*" },
    callback = function()
        vim.opt.expandtab = false
    end
})
-- Disable line numbering in a nested terminal
vim.api.nvim_create_autocmd("TermOpen", {
    callback = function()
        vim.opt.number = false
        vim.opt.relativenumber = false
    end
})
