### EXPORT
PATH="${HOME}/.bin:${PATH}"
export EDITOR="/usr/bin/nvim"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

autoload -U colors && colors

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE="${XDG_STATE_HOME}/zsh/history"
export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)"
setopt hist_ignore_dups

# Basic auto/tab complete:
autoload -U compinit promptinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
promptinit; prompt gentoo
_comp_options+=(globdots)		# Include hidden files.

### SET MANPAGER
### "nvim" as manpager
export MANPAGER='nvim +Man!'
#export MANWIDTH=999

### SET VI MODE ###
# Comment this line out to enable default emacs-like bindings
bindkey -v
# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char
# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

### ALIASES ###
[[ -f "${XDG_CONFIG_HOME}/sh/aliasrc" ]] && source "${XDG_CONFIG_HOME}/sh/aliasrc"

### PROPER TTY FOR GPG
GPG_TTY="$(tty)"
export GPG_TTY

### Unset SSH_ASKPASS
unset SSH_ASKPASS

### SETTING THE STARSHIP PROMPT ###
eval "$(starship init zsh)"

# Syntax highlighting
source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
